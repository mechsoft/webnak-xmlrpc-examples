﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CookComputing.XmlRpc;

namespace OdooServiceTests
{

    [XmlRpcUrl("http://localhost:8069/xmlrpc/common")]
    public interface IOpenErpLogin : IXmlRpcProxy
    {
        [XmlRpcMethod("login")]
        int login(string dbName, string dbUser, string dbPwd);
    }

    public interface IOdooObject : IXmlRpcProxy
    {
        [XmlRpcMethod("execute")]
        int Create(string dbname, int uid, string pwd, string obj, string method, XmlRpcStruct strct);
        [XmlRpcMethod("execute")]
        XmlRpcStruct[] Read(string dbname, int uid, string pwd, string obj, string method, int[] ids, string[] fields);
        [XmlRpcMethod("execute")]
        int[] Search(string dbname, int uid, string pwd, string obj, string method, object[] filter);
        [XmlRpcMethod("execute")]
        XmlRpcStruct[] SearchRead(string dbname, int uid, string pwd, string obj, string method, object[] filter, string[] fields);
        [XmlRpcMethod("execute")]
        bool Unlink(string dbname, int uid, string pwd, string obj, string method, int[] ids);
        [XmlRpcMethod("execute")]
        bool Write(string dbname, int uid, string pwd, string obj, string method, int[] ids, XmlRpcStruct strct);
    }




    [TestClass]
    public class OdooTests
    {
        [TestMethod]
        public void Login()
        {

            IOpenErpLogin rpcClientLogin = XmlRpcProxyGen.Create<IOpenErpLogin>();

            int uid = rpcClientLogin.login("odoodev", "admin", "1");
        }

        [TestMethod]
        public void RunTests()
        {


            IOpenErpLogin rpcClientLogin = XmlRpcProxyGen.Create<IOpenErpLogin>();
            int uid = rpcClientLogin.login("odoodev", "admin", "1");


            IOdooObject odooProxyObject = XmlRpcProxyGen.Create<IOdooObject>();
            odooProxyObject.Url = "http://localhost:8069/xmlrpc/2/object";

            //Create New Case Type
            var caseTypeFields = new XmlRpcStruct();
            caseTypeFields.Add("valueid", "case88");
            caseTypeFields.Add("name", "Highbed");

            var caseCreated = odooProxyObject.Create("odoodev", uid, "1", "webnak.valuelist.casetype", "create", caseTypeFields);

            //Unlink Created Case Type
            odooProxyObject.Unlink("odoodev", uid, "1", "webnak.valuelist.casetype", "unlink", new int[] { caseCreated });

            //Read
            XmlRpcStruct[] partners = odooProxyObject.Read("odoodev", uid, "1", "res.partner", "read", new int[] { 0, 1, 2, 4, 30 }, new string[] { "id", "name" });


            //Search
            object[] searchArgs = new Object[1]; Object[] subargs = new Object[3]; subargs[0] = "webnak_company_type"; subargs[1] = "="; subargs[2] = "yukveren";
            searchArgs[0] = subargs;

            int[] foundPartners = odooProxyObject.Search("odoodev", uid, "1", "res.partner", "search", searchArgs);

            //Search & Read
            XmlRpcStruct[] partnerData = odooProxyObject.SearchRead("odoodev", uid, "1", "res.partner", "search_read", searchArgs, new string[] { "id", "name" });

            //Read Data
            foreach (var item in partnerData)
            {
                var partnerId = item["id"];
                var partnerName = item["name"];
            }

            //Upate Data
            var dataToUpdate = new XmlRpcStruct();
            dataToUpdate.Add("webnak_company_type", "yukalan");

            var updated = odooProxyObject.Write("odoodev", uid, "1", "res.partner", "write", foundPartners, dataToUpdate);

            //Create New Company
            var datatoCreate = new XmlRpcStruct();
            datatoCreate.Add("webnak_company_name", "Fırat Temizlik İşleri");
            datatoCreate.Add("webnak_company_type", "yukalan");
            datatoCreate.Add("is_company", true);

            var created = odooProxyObject.Create("odoodev", uid, "1", "res.partner", "create", datatoCreate);




            //Delete Created Company
            var deleted = odooProxyObject.Unlink("odoodev", uid, "1", "res.partner", "unlink", new int[] { created });

        }
    }
}
